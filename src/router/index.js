import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export const routes = [
	{
		path: '/',
		name: '',
		component: () => import('@/pages/Login.vue')
	},
	{
		path: '/login',
		name: 'login',
		component: () => import('@/pages/Login.vue')
	},
	{
		path: '/dashboard',
		component: () => import(/* webpackChunkName: "layout-default" */ '@/layouts/AppLayout.vue'),
		children: [
			{
				path: '',
				name: 'Dashboard',
				component: () => import(/* webpackChunkName: "starter-page" */ '@/pages/dashboard/DashBoard.vue')
			}
		]
	},
	// {
	//   path: '/previewreport/:testno',
	//   name: 'PreviewReport',
	//   component: () => import(/* webpackChunkName: "starter-page" */ '@/pages/report/PreviewReport.vue'),
	//   props(route) {
	//     return route.query || {}
	//   }
	// },
	{
		path: '/previewreport/:testno',
		name: 'PreviewReport',
		component: () => import(/* webpackChunkName: "starter-page" */ '@/pages/report/PreviewReport.vue'),
		props(route) {
			return route.query || {}
		}
	},
	{
		path: '/testreports',
		component: () => import(/* webpackChunkName: "layout-default" */ '@/layouts/AppLayout.vue'),
		children: [
			{
				path: '',
				name: 'TestReports',
				component: () => import(/* webpackChunkName: "starter-page" */ '@/pages/report/TestReports.vue')
			},
			{
				path: 'editreport',
				name: 'EditReport',
				component: () => import(/* webpackChunkName: "starter-page" */ '@/pages/report/EditReport.vue')
			}
		]
	},
	{
		path: '/setting',
		component: () => import(/* webpackChunkName: "layout-default" */ '@/layouts/AppLayout.vue'),
		children: [
			{
				path: 'user',
				name: 'User',
				component: () => import(/* webpackChunkName: "starter-page" */ '@/pages/setting/User.vue')
			},
			{
				path: 'customers',
				name: 'Customers',
				component: () => import(/* webpackChunkName: "starter-page" */ '../pages/setting/Customers.vue')
			},
			{
				path: 'equipments',
				name: 'Equipments',
				component: () => import(/* webpackChunkName: "starter-page" */ '@/pages/setting/Equipments.vue')
			},
			{
				path: 'teststandard',
				name: 'TestStandard',
				component: () => import(/* webpackChunkName: "starter-page" */ '@/pages/setting/TestStandard.vue')
			},
			{
				path: 'comparegrade',
				name: 'CompareGrade',
				component: () => import(/* webpackChunkName: "starter-page" */ '@/pages/setting/CompareGrade.vue')
			},
			{
				path: 'microstructure',
				name: 'Microstructure',
				component: () => import(/* webpackChunkName: "starter-page" */ '../pages/setting/Microstructure.vue')
			},
			{
				path: 'macrostructure',
				name: 'Macrostructure',
				component: () => import(/* webpackChunkName: "starter-page" */ '../pages/setting/Macrostructure.vue')
			},
			{
				path: 'testlocation',
				name: 'TestLocation',
				component: () => import(/* webpackChunkName: "starter-page" */ '../pages/setting/TestLocation.vue')
			},
			{
				path: 'testscales',
				name: 'TestScales',
				component: () => import(/* webpackChunkName: "starter-page" */ '../pages/setting/TestScales.vue')
			},
			{
				path: 'approvename',
				name: 'Approvename',
				component: () => import(/* webpackChunkName: "starter-page" */ '../pages/setting/Approve.vue')
			},
			{
				path: 'additiondetails',
				name: 'AdditionDetails',
				component: () => import(/* webpackChunkName: "starter-page" */ '../pages/setting/AdditionDetails.vue')
			},
			{
				path: 'remarks',
				name: 'Remarks',
				component: () => import(/* webpackChunkName: "starter-page" */ '../pages/setting/Remarks.vue')
			}
		]
	},
	{
		path: '*',
		component: () => import(/* webpackChunkName: "layout-error" */ '@/layouts/ErrorLayout.vue'),
		children: [
			{
				path: '',
				name: 'error',
				component: () => import(/* webpackChunkName: "error" */ '@/pages/NotFoundPage.vue')
			}
		]
	}
]

const router = new Router({
	// mode: 'history',
	base: process.env.BASE_URL || '/',
	scrollBehavior(to, from, savedPosition) {
		return { x: 0, y: 0 }
	},
	routes
})

/**
 * Before each route update
 */
router.beforeEach((to, from, next) => {
	return next()
})

/**
 * After each route update
 */
router.afterEach((to, from) => {})

export default router
