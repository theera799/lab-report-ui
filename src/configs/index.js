import icons from './icons'
import theme from './theme'

export default {
  // product display information
  product: {
    name: 'Labview',
    version: 'v1.0.0'
  },

  // icon libraries
  icons,

  // theme configs
  theme
}
