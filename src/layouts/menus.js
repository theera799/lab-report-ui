export const menuAdmin = [
	{
		// text: '',
		// items: [
		//   { icon: 'fa-th-large', key: 'menu.dashboard', text: 'Dashboard' },
		//   { icon: 'fa-file-alt', key: 'menu.blank', text: 'Reports', link: '' },
		//   { icon: 'fa-cog', key: 'menu.blank', text: 'Setting', link: '' }
		//   // { icon: 'fa-user', key: 'menu.blank', text: 'Customers', link: '' },
		//   // { icon: 'fa-industry', key: 'menu.blank', text: 'Equipments', link: '' },
		//   // { icon: 'fa-list-ol', key: 'menu.blank', text: 'Logs', link: '' },

		// ]
		text: '',
		// key: 'menu.others',
		items: [
			{
				icon: 'icon-view-grid-outline',
				key: 'menu.dashboard',
				text: 'Dashboard',
				link: '/dashboard'
			},
			{
				icon: 'icon-folder',
				key: 'menu.report',
				text: 'Test Reports',
				link: '/testreports'
			},
			{
				icon: 'icon-cog-outline',
				key: 'menu.levels',
				text: 'Setting',
				items: [
					{
						key: 'menu.customers',
						text: 'Customers',
						link: '/setting/customers'
					},
					{
						key: 'menu.equipments',
						text: 'Equipments',
						link: '/setting/equipments'
					},
					{
						key: 'menu.additiondetails',
						text: 'Addition Detail',
						link: '/setting/additiondetails'
					},
					{
						key: 'menu.testscales',
						text: 'Approve Name',
						link: '/setting/approvename'
					},
					{
						key: 'menu.comparegrade',
						text: 'Compare Grade',
						link: '/setting/comparegrade'
					},
					// { key: 'menu.masterreports', text: 'Master Reports' },
					{
						key: 'menu.macrostructure',
						text: 'Macrostructure',
						link: '/setting/macrostructure'
					},
					{
						key: 'menu.microstructure',
						text: 'Microstructure',
						link: '/setting/microstructure'
					},
					// { key: 'menu.remark', text: 'Remark', link: '/setting/remark' },
					{
						key: 'menu.testlocation',
						text: 'Test Location',
						link: '/setting/testlocation'
					},
					{
						key: 'menu.testscales',
						text: 'Test Scales',
						link: '/setting/testscales'
					},
					{
						key: 'menu.teststandard',
						text: 'Test Standard',
						link: '/setting/teststandard'
					},

					{ key: 'menu.remarks', text: 'Remarks', link: '/setting/remarks' }
				]
			},
			{
				icon: 'mdi-logout',
				key: 'menu.dashboard',
				text: 'Logout',
				link: '/login'
			}
		]
	}
]

// {
//   title: 'Home',
//   link: '/home'
// }, {
//   title: 'Dashboard',
//   link: '/dashboard'
// }, {
//   title: 'Monitor',
//   link: '/monitor'
// }

export const menuUser = [
	{
		title: 'Product',
		link: '#product'
	},
	{
		title: 'Features',
		link: '#features'
	},
	{
		title: 'Marketplace',
		link: '#marketplace'
	},
	{
		title: 'Company',
		link: '#company'
	}
]

export default {
	menuAdmin,
	menuUser
}
