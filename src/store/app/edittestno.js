const state = {
  testNo: '555'
}

const getters = {
}

const actions = {
  setTestNo: ({ commit, state }, payload) => {
    commit('SET_TEST_NO', payload.testNo)
  }
}

const mutations = {
  SET_TEST_NO(state, payload) {
    state.testNo = payload
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
