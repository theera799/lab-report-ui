import Vue from 'vue'
import Vuex from 'vuex'
import { getField, updateField } from 'vuex-map-fields'
import createPersistedState from 'vuex-persistedstate'

// Global vuex
import AppModule from './app'
import editTestNo from './app/edittestno'

Vue.use(Vuex)

/**
 * Main Vuex Store
 */
const store = new Vuex.Store({
  modules: {
    editTestNo,
    app: AppModule
  },
  plugins: [createPersistedState()],
  state: {
    controlValue: {
      showLayout: true,
      floatLayout: true,
      enableDownload: true,
      previewModal: true,
      paginateElementsByHeight: 1100,
      manualPagination: false,
      filename: 'Hee Hee',
      pdfQuality: 2,
      pdfFormat: 'a4',
      pdfOrientation: 'portrait',
      pdfContentWidth: '800px'
    }
  },

  mutations: {
    updateField
  },

  getters: {
    getField
  }

})

export default store
