/* eslint-disable space-before-blocks */
/* eslint-disable indent */
import { HTTP } from '@/axios.js'
import appConst from '/public/const.json'
//import Vtoast from '../utils/snackbar.vue'

export default {
	components: {
		//Vtoast
	},
	methods: {

		// =====================================================================================
		//
		//									Report Preview
		//
		// =====================================================================================
		/*

			/*var splitdate = this.test_date.split(' ')
			if (splitdate[0].includes("-")) {
				let ndate = moment(splitdate[0], 'DD-MMM-YY')
				this.fdate = ndate.format('YYYY-MM-DD')
			} else {
				let ndate = moment(splitdate[0], 'DD/MM/YYYY')
				this.fdate = ndate.subtract(543, 'years').format('YYYY-MM-DD')
			}
		*/

		getConstReportTitleFromType(form_type) {
			switch(form_type)
			{
				case "PL":
				case "TS": return appConst.reportTitleTS
				case "BD": return appConst.reportTitleBD
				case "FTN": return appConst.reportTitleFTN
				case "NB": return appConst.reportTitleNB
				case "FT": return appConst.reportTitleFT
				case "IM": return appConst.reportTitleIM
				case "MA": return appConst.reportTitleMA
				case "MI": return appConst.reportTitleMI
				case "ES": return appConst.reportTitleES
				case "MS": return appConst.reportTitleMS
				case "HV": return appConst.reportTitleHV
				case "HR": return appConst.reportTitleHR
				case "HB": return appConst.reportTitleHB
				case "PC": return appConst.reportTitlePC
				case "IC": return appConst.reportTitleIC
				case "MM": return appConst.reportTitleMM
				case "DM": return appConst.reportTitleDM
			}
			return "N/A"
		},

		DateTimeToYMD(dt) {
			return dt.substring(0, 10);
		},

		FilterDateTime(str) {

			if(str === "" || str === null) return ""

			if(typeof str === 'undefined') return ""

			var str_arr = str.split(" ");
			var str_arr2 = str.split("/");
			var str_arr3 = str.split("-");

			str_arr3[0] = parseInt(str_arr3[0])
			str_arr3[1] = parseInt(str_arr3[1])
			str_arr3[2] = parseInt(str_arr3[2])

			str_arr2[0] = parseInt(str_arr2[0])
			str_arr2[1] = parseInt(str_arr2[1])
			str_arr2[2] = parseInt(str_arr2[2])

			var d = new Date(),
			//minutes = d.getMinutes().toString().length == 1 ? '0'+d.getMinutes() : d.getMinutes(),
			//hours = d.getHours().toString().length == 1 ? '0'+d.getHours() : d.getHours(),
			//ampm = d.getHours() >= 12 ? 'pm' : 'am',
			months = ['January','February','March','April','May','June','July','August','September','October','November','December'],
			days = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
			//return days[d.getDay()]+' '+months[d.getMonth()]+' '+d.getDate()+' '+d.getFullYear()+' '+hours+':'+minutes+ampm;
			
			// Format YYYY-MM-DD
			if(	str_arr3[0] > 2000 &&
				str_arr3[1] >= 1 && str_arr3[1] <= 12 &&
				str_arr3[2] >= 1 && str_arr3[1] <= 31 )
			{
				if(str_arr3[0] > 2500){
					var tmp = str_arr3[2]+" "+months[str_arr3[1]-1]+" "+(str_arr3[0]-543)
					return tmp
				}
				else{
					var tmp = str_arr3[2]+" "+months[str_arr3[1]-1]+" "+str_arr3[0]
					return tmp
				}
			}

			// Format D/M/YYYY
			return str_arr2[0]+" "+months[str_arr2[1]-1]+" "+(parseInt(str_arr2[2])-543)
		},

		FilterBlank(str, digit = -1) {
			if(!str || str === ""){
				return "-"
			}
			if(digit === -1){
				return str
			}
			return parseFloat(str).toFixed(digit)
		},

		GetNumberFromBeforeString(text) {
			var result = "";
			for (var i = 0; i < text.length; i++) {
				var code = text.charCodeAt(i)
				if (code >= 48 && code <= 57) //code 0 - 9
				{
					result += text[i];
				}
				else
				{
					break;
				}
			}

			return result;
		},

		GetStringFromAfterNumber(text) {
			var result = "";

			for (var i = 0; i < text.length; i++) {
				var code = text.toUpperCase().charCodeAt(i)
				if(code >= 65 && code <= 90) // code A - Z
				{
					for (var j = i; j < text.length; j++) {
						result += text[j]
					}
					return result
				}
			}

			return result;
		},

		GetStringOnlyFromAfterNumber(text) {
			var result = "";

			for (var i = 0; i < text.length; i++) {
				var code = text.toUpperCase().charCodeAt(i)
				if(code >= 65 && code <= 90) // code A - Z
				{
					for (var j = i; j < text.length; j++) {
						var c = text.toUpperCase().charCodeAt(j)
						if(c >= 65 && code <= 90) // code A - Z 
						{
							result += text[j]
						}
						else
						{
							break
						}
						
					}
					return result
				}
			}

			return result;
		},
		
		IsReportNoCorrect01(str) {
			var str_arr = str.split("-");
			return !isNaN(str_arr)
		},
		mergeSimpleNo(str1, str2){
			var str_arr = str1.split("-");
			if(str_arr.length >= 3){
				return str1+str2
			}

			return str1+"-"+str2
		},

		FilterReportNo_AAA_BB(str) {
			var str_arr = str.split("-");
			if(str_arr.length >= 2) return str_arr[0] + "-" + str_arr[1]
			else if(str_arr.length === 1) return str_arr[0]
			return ""
		},

		FilterReportNo_AAA_BB_C(str) {
			var str_arr = str.split("-");
			if(str_arr.length <= 2) return str_arr[0] + "-" + str_arr[1]
			var res = this.GetNumberFromBeforeString(str_arr[2])
			if(res === "") 
				return str_arr[0] + "-" + str_arr[1]

			return str_arr[0] + "-" + str_arr[1] + "-" + res
		},

		FilterReportNo_AAA_BB_Cxx(str, convtype = true) {
			var str_arr = str.split("-");

			var AAA = str_arr[0]
			var BB = str_arr[1]

			//AAA-BB
			if(str_arr.length <= 2) return AAA + "-" + BB

			var CxxD = str_arr[2]

			var C = this.GetNumberFromBeforeString(CxxD)
			var xx = this.GetStringOnlyFromAfterNumber(str_arr[2])
			if(convtype) xx = this.getFormTypeByXX(xx)

			if(C === ""){
				//AAA-BB-xx
				return AAA + "-" + BB + "-" + xx
			}
			else{
				if(xx === ""){
					//AAA-BB-C
					return AAA + "-" + BB + "-" + C
				}
			}

			//AAA-BB-Cxx
			return AAA + "-" + BB + "-" + C + xx
		},
		FilterReportNo_AAA_BBxx_Or_AAA_BB_Cxx(str, convtype = true) {
			var str_arr = str.split("-");

			var AAA = str_arr[0]
			var BB = str_arr[1]

			//AAA-BB
			if(str_arr.length <= 2) return AAA + "-" + BB

			var CxxD = str_arr[2]

			var C = this.GetNumberFromBeforeString(CxxD)
			var xx = this.GetStringOnlyFromAfterNumber(str_arr[2])
			if(convtype) xx = this.getFormTypeByXX(xx)

			if(C === ""){
				//AAA-BB-xx
				return AAA + "-" + BB + xx
			}
			else{
				if(xx === ""){
					//AAA-BB-C
					return AAA + "-" + BB + "-" + C
				}
			}

			//AAA-BB-Cxx
			return AAA + "-" + BB + "-" + C + xx
		},
		FilterReportNo_xxD(str) {
			var str_arr = str.split("-");
			if(str_arr.length <= 2) return ""
			var res = this.GetStringFromAfterNumber(str_arr[2])
			return res
		},

		getFormTypeByXX(form_type) {

			var form_list = [
				["TS", "TS"],
				["TSW", "TS"],
				["TSWG1", "TS"],
				["TSB1", "TS"],
				["TSB1G1", "TS"],
				["TSB2", "TS"],
				["TSB2G1", "TS"],
				["TSB3", "TS"],
				["TSB3G1", "TS"],
				["TSPL", "TS"],
				["TSPLG1", "TS"],

				["BD", "BD"],
				["BDMAT", "BD"],
				["FB", "BD"],
				["RB", "BD"],
				["SB", "BD"],

				["IM", "IM"],

				["ES", "ES"],
				["ESSTD", "ES"],
				["ESAERO", "ES"],

				["HV", "HV"],
				["HV3COL", "HV"],
				["HV4COL", "HV"],
				["HVPIC", "HV"],

				["HR", "HR"],
				["HB", "HB"],

				["MI", "MI"],

				["MS", "MS"],
				["MS1", "MS"],
				["MS1STD", "MS"],
				["MS2", "MS"],
				["MS2STD", "MS"],

				["FTN", "FTN"],
				["FT", "FT"],

				["NB", "NB"],
				["IM", "IM"],
				["MA", "MA"],
				["PC", "PC"],
				["IC", "IC"],
				["MM", "MM"],
				["DM", "DM"],
			]

			for(var i = 0; i < form_list.length; i++)
			{
				if(form_type === form_list[i][0]) return form_list[i][1]
			}

			return form_type
		},

		BD_SpecType(form_type2) {
			if( form_type2.search("FB") >= 0) return "Face bend"
			else if( form_type2.search("RB") >= 0) return "Root bend"
			else if( form_type2.search("SB") >= 0) return "Side bend"
			else if( form_type2.search("BD") >= 0) return "Base metal"
			return "-"
		},

		// =====================================================================================
		//
		//									Utility
		//
		// =====================================================================================

		showNotic(title, type = "success", text = "", group = "foo-css") {
			/*this.$notify({
				group,
				title: `${type} notification `,
				text,
				type,
				data: {
					randomNumber: Math.random(),
				},
			})*/
			this.$root.vtoast.show({ message: title, color: type })
		},

		getConstCompName() {
			return appConst.compName
		},
		getConstManagerName() {
			return appConst.managerName
		},
		getConstManagerPosition() {
			return appConst.managerPosition
		},
		getConstTestTemp() {
			return appConst.testTemp
		},
		getConstTestHumidity() {
			return appConst.testHumidity
		},

		/*getConstMagnifiList(type) {
			return appConst.magnifiList
		},*/

		getConstHvDrwUrl() {
			return appConst.hvdrwUrl
		},

		getConstEditResult(type) {
			switch(type)
			{
				case "remark":
					return appConst.customerEditResult.remark
				break
				case "mi_remark":
					return appConst.customerEditResult.mi_remark
				break
				case "location":
					return appConst.customerEditResult.location
				break
				case "ma_macro":
					return appConst.customerEditResult.ma_macro
				break
				case "mi1_micro":
					return appConst.customerEditResult.mi1_micro
				break
				case "mi2_micro":
					return appConst.customerEditResult.mi2_micro
				break
			}
			return []
		},
		getConstTswGTestRate() {
			return appConst.tswGTestRate
		},
		getConstTswGTemp() {
			return appConst.tswGTemp
		},
		getConstTswGFirstTestRate() {
			return appConst.tswGFirstTestRate
		},
		getConstTswGSecondTestRate() {
			return appConst.tswGSecondTestRate
		},

		getGraphImgPath(index) {
			switch(index)
			{
				case 0: return appConst.graphImgPath1
				case 1: return appConst.graphImgPath2
				case 2: return appConst.graphImgPath3
				case 3: return appConst.graphImgPath4
				case 4: return appConst.graphImgPath5
				case 5: return appConst.graphImgPath6
				case 6: return appConst.graphImgPath7
				case 7: return appConst.graphImgPath8
				case 8: return appConst.graphImgPath9
				case 9: return appConst.graphImgPath10
			}
		},

		getConstMaEetchReag() {
			return appConst.maEetchReag
		},
		getConstMaEetchTemp() {
			return appConst.maEetchTemp
		},
		getConstMaEetchTime() {
			return appConst.maEetchTime
		},

		getConstMiEetchReag() {
			return appConst.miEetchReag
		},
		getConstMiEetchTime() {
			return appConst.miEetchTime
		},
		getConstMiMagnifi() {
			return appConst.miMagnifi
		},

		getConstFcEetchReag() {
			return appConst.fcEetchReag
		},
		getConstFcEetchTime() {
			return appConst.fcEetchTime
		},
		getConstFcMagnifi() {
			return appConst.fcMagnifi
		},

		getConstHvTestForce() {
			return appConst.hvTestForce
		},
		getConstHrTestForce() {
			return appConst.hrTestForce
		},
		getConstHbTestForce() {
			return appConst.hbTestForce
		},

		getConstPcSolution() {
			return appConst.pcSolution
		},
		getConstPcHolding() {
			return appConst.pcHolding
		},

		getConstIcSolution() {
			return appConst.icSolution
		},
		getConstIcHolding() {
			return appConst.icHolding
		},




		// =====================================================================================
		//
		//									API Call
		//
		// =====================================================================================

		
		async getTestForceListByType (report_type){
			try {
				const config = {
					url: localStorage.getItem('apiurl') + 'testscaleslist',
					method: 'GET',
					headers: {
						Authorization: 'Bearer ' + this.userToken
					},
					params: {
						'tstype': report_type
					},
				}
				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('getTestForceListByType success ', response)

				if(response.data !== null && response.data.length > 0) 
					return this.ObjectLoopDecodeJson(response.data)
				else 
					return []
			} catch (error) {
				return error
			}
		},

		async getCustomMagnifi (report_type){
			try {
				const config = {
					url: localStorage.getItem('apiurl') + 'remarklist',
					method: 'GET',
					headers: {
						Authorization: 'Bearer ' + this.userToken
					},
					params: {
						'rmtype': 'remark_list',
						'testtype': 'MAG'
					},
				}
				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('getCustomMagnifi success ', response)

				if(response.data !== null && response.data.length > 0) 
					return this.ObjectLoopDecodeJson(response.data)
				else 
					return []
			} catch (error) {
				return error
			}
		},

		async getCustomRemark (){
			try {
				const config = {
					url: localStorage.getItem('apiurl') + 'remarklist',
					method: 'GET',
					headers: {
						Authorization: 'Bearer ' + this.userToken
					},
					params: {
						'rmtype': 'remark_list',
						'testtype': 'ALL'
					},
				}
				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('getCustomRemark success ', response)

				if(response.data !== null && response.data.length > 0) 
					return this.ObjectLoopDecodeJson(response.data)
				else 
					return []
			} catch (error) {
				return error
			}
		},

		async getCustomRemarkMI (){
			try {
				const config = {
					url: localStorage.getItem('apiurl') + 'remarklist',
					method: 'GET',
					headers: {
						Authorization: 'Bearer ' + this.userToken
					},
					params: {
						'rmtype': 'remark_list',
						'testtype': 'MI'
					},
				}
				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('getCustomRemarkMI success ', response)

				if(response.data !== null && response.data.length > 0) 
					return this.ObjectLoopDecodeJson(response.data)
				else 
					return []
			} catch (error) {
				return error
			}
		},

		async getCustomMacro (){
			try {
				const config = {
					url: localStorage.getItem('apiurl') + 'macrolist',
					method: 'GET',
					headers: {
						Authorization: 'Bearer ' + this.userToken
					},
				}
				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('getCustomMacro success ', response)

				if(response.data !== null && response.data.length > 0) 
					return this.ObjectLoopDecodeJson(response.data)
				else 
					return []
			} catch (error) {
				return error
			}
		},

		async getCustomMicro (){
			try {
				const config = {
					url: localStorage.getItem('apiurl') + 'microlist',
					method: 'GET',
					headers: {
						Authorization: 'Bearer ' + this.userToken
					},
				}
				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('getCustomMicro success ', response)

				if(response.data !== null && response.data.length > 0) 
					return this.ObjectLoopDecodeJson(response.data)
				else 
					return []
			} catch (error) {
				return error
			}
		},

		async getCustomLocationTS (){
			try {
				const config = {
					url: localStorage.getItem('apiurl') + 'testlocationlist',
					method: 'GET',
					headers: {
						Authorization: 'Bearer ' + this.userToken
					},
					params: {
						'tltype': 'TSW',
					},
				}
				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('getCustomLocationTS success ', response)

				if(response.data !== null && response.data.length > 0) 
					return this.ObjectLoopDecodeJson(response.data)
				else 
					return []
			} catch (error) {
				return error
			}
		},

		async getCustomLocationMI (){
			try {
				const config = {
					url: localStorage.getItem('apiurl') + 'testlocationlist',
					method: 'GET',
					headers: {
						Authorization: 'Bearer ' + this.userToken
					},
					params: {
						'tltype': 'MI',
					},
				}
				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('getCustomLocationMI success ', response)

				if(response.data !== null && response.data.length > 0) 
					return this.ObjectLoopDecodeJson(response.data)
				else 
					return []
			} catch (error) {
				return error
			}
		},

		async getCompareGradeListByReportType2Digit (report_type){
			try {
				const config = {
					url: localStorage.getItem('apiurl') + 'comparelist',
					method: 'GET',
					headers: {
						Authorization: 'Bearer ' + this.userToken
					}
				}
				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('getCompareGradeListByReportType2Digit success ', response)

				if(response.data !== null && response.data.length > 0) 
				{
					var data = response.data
					var tmp = data.filter(function(obj) {
						for(var n = 0; n < obj.cglink.length; n++)
						{
							return obj.cglink[n].includes(report_type)
						}
					})
					return this.ObjectLoopDecodeJson(tmp)
				}
				else {
					return []
				}
			} catch (error) {
				return error
			}
		},
		async getResultOptionByResultNo (res_no){
			try {
				const config = {
					url: localStorage.getItem('apiurl') + 'resultadditionallist',
					method: 'POST',
					headers: {
						Authorization: 'Bearer ' + this.userToken
					},
					params: {
						'result_no': res_no
					},
				}
				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('getResultOptionByResultNo success ', response)

				if(response.data !== null && response.data.length > 0) 
				{
					return this.ObjectLoopDecodeJson(response.data)
				}
				else return []
			} catch (error) {
				return error
			}
		},
		async getResultOptionByKey (name){
			try {
				const config = {
					url: localStorage.getItem('apiurl') + 'resultadditionallist',
					method: 'POST',
					headers: {
						Authorization: 'Bearer ' + this.userToken
					},
					params: {
						'name': name,
					},
				}
				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('getResultOptionByResNo success ', response)

				if(response.data !== null && response.data.length > 0) 
				{
					return this.ObjectLoopDecodeJson(response.data)
				}
				else return []
			} catch (error) {
				return error
			}
		},
		async editResultOptionByKey (res_no, name, value){
			try {
				const config = {
					url: localStorage.getItem('apiurl') + 'resultadditionaladd',
					method: 'GET',
					headers: {
						Authorization: 'Bearer ' + this.userToken
					},
					params: {
						'result_no': res_no,
						'name': name,
						'value': value,
					},
				}
				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('getResultOptionByResNo success ', response)

				if(response.data !== null && response.data.status === "success") return true
				else return false
			} catch (error) {
				return error
			}
		},
		async getCustomerList (){
			try {
				const config = {
					url: localStorage.getItem('apiurl') + 'customerlist',
					method: 'POST',
					headers: {
						Authorization: 'Bearer ' + this.userToken
					}
				}
				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('getCustomerList success ', response)

				if(response.data !== null && response.data.length > 0) 
				{
					return this.ObjectLoopDecodeJson(response.data)
				}
				else return []
			} catch (error) {
				return error
			}
		},
		async getProjectGroupList (){
			try {
				const config = {
					url: localStorage.getItem('apiurl') + 'projectshortlist',
					method: 'POST',
					headers: {
						Authorization: 'Bearer ' + this.userToken
					}
				}
				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('getProjectGroupList success ', response)

				if(response.data !== null && response.data.length > 0) 
				{
					return this.ObjectLoopDecodeJson(response.data)
				}
				else return []
			} catch (error) {
				return error
			}
		},
		async getProjectListByTestNo6Digit (testno6digit){
			try {
				const config = {
					url: localStorage.getItem('apiurl') + 'projectlist',
					method: 'POST',
					headers: {
						Authorization: 'Bearer ' + this.userToken
					}
				}
				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('getProjectListByTestNo6Digit success ', response)

				if(response.data !== null && response.data.length > 0) 
				{
					var data = response.data
					var tmp = data.filter(function(obj) {
						return (obj.test_no.trim().substring(0, 6) === testno6digit)
					})

					if(tmp !== null && tmp.length > 0) 
					{
						for(var i = 0; i < tmp.length; i++)
						{
							if (tmp[i].receivedate === "2000-01-01")
								tmp[i].receivedate = ""
							if (tmp[i].test_date === "2000-01-01")
								tmp[i].test_date = ""
							if (tmp[i].calibration_date === "2000-01-01")
								tmp[i].calibration_date = ""
							if (tmp[i].createdate === "2000-01-01")
								tmp[i].createdate = ""
							if (tmp[i].updatedate === "2000-01-01")
								tmp[i].updatedate = ""
							if (tmp[i].app_date === "2000-01-01")
								tmp[i].app_date = ""
							if (tmp[i].client_date === "2000-01-01")
								tmp[i].client_date = ""
							if (tmp[i].review_date1 === "2000-01-01")
								tmp[i].review_date1 = ""
							if (tmp[i].review_date2 === "2000-01-01")
								tmp[i].review_date2 = ""
							if (tmp[i].owner_date === "2000-01-01")
								tmp[i].owner_date = ""
						}

						return this.ObjectLoopDecodeJson(tmp)
					}
				}
				else {
					return []
				}
			} catch (error) {
				return error
			}
		},
		async getProjectInfoByTestNo6Digit (testno6digit){
			try {
				const config = {
					url: localStorage.getItem('apiurl') + 'projectlist',
					method: 'GET',
					headers: {
						Authorization: 'Bearer ' + this.userToken
					},
					params: {
						'test_no': testno6digit
					},
				}
				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('getProjectInfoByTestNo6Digit success ', response)

				if(response.data !== null && response.data.length > 0) 
				{
					if (response.data[0].receivedate === "2000-01-01")
						response.data[0].receivedate = ""
					if (response.data[0].test_date === "2000-01-01")
						response.data[0].test_date = ""
					if (response.data[0].calibration_date === "2000-01-01")
						response.data[0].calibration_date = ""
					if (response.data[0].createdate === "2000-01-01")
						response.data[0].createdate = ""
					if (response.data[0].updatedate === "2000-01-01")
						response.data[0].updatedate = ""
					if (response.data[0].app_date === "2000-01-01")
						response.data[0].app_date = ""
					if (response.data[0].client_date === "2000-01-01")
						response.data[0].client_date = ""
					if (response.data[0].review_date1 === "2000-01-01")
						response.data[0].review_date1 = ""
					if (response.data[0].review_date2 === "2000-01-01")
						response.data[0].review_date2 = ""
					if (response.data[0].owner_date === "2000-01-01")
						response.data[0].owner_date = ""

					return this.ObjectLoopDecodeJson(response.data[0])
				}
				else return null
			} catch (error) {
				return error
			}
		},
		async editProjectInfo (projectinfo){

			if (projectinfo.cusid === "") projectinfo.cusid = 0
			if (projectinfo.pname === "") projectinfo.pname = "Project Name"
			if (projectinfo.pno === "") projectinfo.pno = "Project No"
			if (projectinfo.att_page === "") projectinfo.att_page = 0

			if (projectinfo.receivedate === "")
				projectinfo.receivedate = "2000-01-01"
			if (projectinfo.test_date === "")
				projectinfo.test_date = "2000-01-01"
			if (projectinfo.calibration_date === "")
				projectinfo.calibration_date = "2000-01-01"
			if (projectinfo.createdate === "")
				projectinfo.createdate = "2000-01-01"
			if (projectinfo.updatedate === "")
				projectinfo.updatedate = "2000-01-01"
			if (projectinfo.app_date === "")
				projectinfo.app_date = "2000-01-01"
			if (projectinfo.client_date === "")
				projectinfo.client_date = "2000-01-01"
			if (projectinfo.review_date1 === "")
				projectinfo.review_date1 = "2000-01-01"
			if (projectinfo.review_date2 === "")
				projectinfo.review_date2 = "2000-01-01"
			if (projectinfo.owner_date === "")
				projectinfo.owner_date = "2000-01-01"

			projectinfo = this.ObjectLoopEncodeJson(projectinfo)


			try {
				let formData = new FormData();
				for (var key in projectinfo) {
					formData.append(key, projectinfo[key])
				}

				const config = {
					url: localStorage.getItem('apiurl') + 'projectedit',
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						'Authorization': 'Bearer ' + this.userToken
					},
					data: formData,
				}
				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('editProjectInfo success ', response)

				if(response.data !== null && response.data.status === "success") return true
				else return false
			} catch (error) {
				return error
			}
		},
		async getReportInfoByTestNo (testno){
			try {
				const config = {
					url: localStorage.getItem('apiurl') + 'getreportinfo',
					method: 'GET',
					headers: {
						Authorization: 'Bearer ' + this.userToken
					},
					params: {
						'test_no': testno
					},
				}
				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('getReportInfoByTestNo success ', response)

				if(response.data !== null && response.data.report_result.length > 0)  {
					//response.data.report_result[0].eqid = parseInt(response.data.report_result[0].eqid)
					//response.data.report_result[0].cgid = parseInt(response.data.report_result[0].cgid);
					//response.data.report_result[0].rid = parseInt(response.data.report_result[0].rid);

					for(var i = 0; i < response.data.report_result.length; i++)
					{
						if (response.data.report_result[i].approve_date === "2000-01-01")
							response.data.report_result[i].approve_date = ""
						if (response.data.report_result[i].client_date === "2000-01-01")
							response.data.report_result[i].client_date = ""
						if (response.data.report_result[i].review1_date === "2000-01-01")
							response.data.report_result[i].review1_date = ""
						if (response.data.report_result[i].review2_date === "2000-01-01")
							response.data.report_result[i].review2_date = ""
						if (response.data.report_result[i].owner_date === "2000-01-01")
							response.data.report_result[i].owner_date = ""
					}

					////////////////////////////////
					
					for(i = 0; i < response.data.report_result.length; i++)
					{
						//response.data.report_result[i].report_no = this.FilterReportNo_AAA_BB_Cxx(response.data.report_result[i].report_no)
						response.data.report_result[i].report_no = this.FilterReportNo_AAA_BBxx_Or_AAA_BB_Cxx(response.data.report_result[i].report_no)
					}
					////////////////////////////////

					return this.ObjectLoopDecodeJson(response.data)
				}
				else {
					return null
				}
			} catch (error) {
				return error
			}
		},
		async getReportById (rid){
			try {
				const config = {
					url: localStorage.getItem('apiurl') + 'reportlist',
					method: 'GET',
					headers: {
						Authorization: 'Bearer ' + this.userToken
					},
					params: {
						'rid': rid
					},
				}
				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('getReportById success ', response)

				if(response.data !== null && response.data.length > 0) return this.ObjectLoopDecodeJson(response.data[0])
				else return null
			} catch (error) {
				return error
			}
		},

		
		ObjectLoopEncodeJson(obj) 
		{
			if (Array.isArray(obj))
			{
				for (var i = 0; i < obj.length; i++) {
					var value1 = obj[i];
					obj[i] = this.ObjectLoopEncodeJson(value1);
				}
				return obj
			}
			else if (typeof obj === 'object') 
			{
				for (var key in obj) {
					var value2 = obj[key];
					obj[key] = this.ObjectLoopEncodeJson(value2);
				}
				return obj
			}
			else
			{
				if(typeof obj === "string")
				{
					obj = obj.replace(/\\/g, '\\\\');
					obj = obj.replace(/"/g, '\\"');
				}
				return obj
			}
		},

		ObjectLoopDecodeJson(obj) 
		{
			if (Array.isArray(obj))
			{
				for (var i = 0; i < obj.length; i++) {
					var value1 = obj[i];
					obj[i] = this.ObjectLoopDecodeJson(value1);
				}
				return obj
			}
			else if (typeof obj === 'object') 
			{
				for (var key in obj) {
					var value2 = obj[key];
					obj[key] = this.ObjectLoopDecodeJson(value2);
				}
				return obj
			}
			else
			{
				if(typeof obj === "string")
				{
					obj = obj.replace(/\\"/g, '"');
					obj = obj.replace(/\\\\/g, '\\');
				}
				return obj
			}
		},

		async editReportInfoById (rid, param){

			if (param.page_index === "") param.page_index = 0
			if (param.adid === "") param.adid = 0
			if (param.eqid === "") param.eqid = 0
			if (param.cgid === "") param.cgid = 0
			//if (param.remarkid === "") param.remarkid = 0
			if (param.page === "") param.page = 0

			if(param.additonal === "") param.additonal = "x"
			if(param.appr_comp === "") param.appr_comp = "x"
			if(param.appr_name === "") param.appr_name = "x"

			if (param.create_date === "")
				param.create_date = "2000-01-01"
			if (param.update_date === "")
				param.update_date = "2000-01-01"
			if (param.app_date === "")
				param.app_date = "2000-01-01"
			if (param.appr_date === "")
				param.appr_date = "2000-01-01"
			if (param.client_date === "")
				param.client_date = "2000-01-01"
			if (param.review_date1 === "")
				param.review_date1 = "2000-01-01"
			if (param.review_date2 === "")
				param.review_date2 = "2000-01-01"
			if (param.owner_date === "")
				param.owner_date = "2000-01-01"

			param = this.ObjectLoopEncodeJson(param)

			try {
				const config = {
					url: localStorage.getItem('apiurl') + 'reportedit',
					method: 'GET',
					headers: {
						Authorization: 'Bearer ' + this.userToken
					},
					params: param,
				}

				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('editReportInfoById success ', response)

				if(response.data !== null && response.data.status === "success") return true
				else return false
			} catch (error) {
				return error
			}
		},

		async getApproveList (){
			try {
				const config = {
					url: localStorage.getItem('apiurl') + 'approvelist',
					method: 'POST',
					headers: {
						Authorization: 'Bearer ' + this.userToken
					}
				}
				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('getApproveList success ', response)

				if(response.data !== null) return this.ObjectLoopDecodeJson(response.data[0].apvname)
				else return []
			} catch (error) {
				return error
			}
		},
		async removeProjectGroupByTestNo6Digit (testno6digit){
			try {
				
				const config = {
					url: localStorage.getItem('apiurl') + 'testnodel',
					method: 'GET',
					headers: {
						Authorization: 'Bearer ' + this.userToken
					},
					params: {
						'test_no': testno6digit
					},
				}
				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('removeProjectGroupByTestNo6Digit ', response.data.status)

				if(response.data !== null) return response.data
				else return {status: false}
			} catch (error) {
				return error
			}
		},
		removeProjectGroupByTestNo6DigitFromArray(projectList, testno6digit) {
			var tmp = projectList.filter(function(obj) {
				return obj.project_no6 !== testno6digit
			})
			return tmp
		},
		getCustomerNameByIdFromArray(customerList, id) {
			for (var i = 0; i < customerList.length; i++) {
				if (parseInt(customerList[i].cusid) === parseInt(id)) return customerList[i].cname
			}
			return ""
		},
		getCustomerAddressByIdFromArray(customerList, id) {
			for (var i = 0; i < customerList.length; i++) {
				if (parseInt(customerList[i].cusid) === parseInt(id)) return customerList[i].caddress
			}
			return ""
		},
		getCompareGradeByIdFromArray(compareList, id) {
			for (var i = 0; i < compareList.length; i++) {
				if (parseInt(compareList[i].cgid) === parseInt(id)) return compareList[i]
			}
			return {}
		},
		async saveReportCustomInfo(param) {
			try {
				const config = {
					url: localStorage.getItem('apiurl') + 'reportformtypecusedit',
					method: 'POST',
					headers: {
						Authorization: 'Bearer ' + this.userToken
					},
					params: param,
				}
				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('saveReportCustomInfo ', response)

				if(response.data !== null && response.data.status === "success") return true
				else return false
			} catch (error) {
				return error
			}
		},
		async getStandardByType (standard_type){
			try {
				if(standard_type === "PL") standard_type = "TS"
				const config = {
					url: localStorage.getItem('apiurl') + 'testsdlist',
					method: 'GET',
					headers: {
						Authorization: 'Bearer ' + this.userToken
					},
					params: {
						'testsdname': standard_type
					},
				}
				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('getStandardByType success ', response)

				if(response.data !== null && response.data.length > 0) return this.ObjectLoopDecodeJson(response.data[0])
				else return null
			} catch (error) {
				return error
			}
		},
		async getEquipListByType (equip_type){
			try {
				if(equip_type === "PL") equip_type = "TS"
				const config = {
					url: localStorage.getItem('apiurl') + 'equiplist',
					method: 'GET',
					headers: {
						Authorization: 'Bearer ' + this.userToken
					},
					params: {
						'eqtype': equip_type,
					},
				}
				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('getEquipListByType success ', response)

				if(response.data !== null) return this.ObjectLoopDecodeJson(response.data)
				else return []
			} catch (error) {
				return error
			}
		},
		async getTestScaleListByType (scale_type){
			try {
				const config = {
					url: localStorage.getItem('apiurl') + 'testscaleslist',
					method: 'GET',
					headers: {
						Authorization: 'Bearer ' + this.userToken
					},
					params: {
						'tstype': scale_type,
					},
				}
				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('getTestScaleListByType success ', response)

				if(response.data !== null) return this.ObjectLoopDecodeJson(response.data)
				else return []
			} catch (error) {
				return error
			}
		},
		async getAdditionByType (remarktype){
			try {
				const config = {
					url: localStorage.getItem('apiurl') + 'remarklist',
					method: 'GET',
					headers: {
						Authorization: 'Bearer ' + this.userToken
					},
					params: {
						//'rmid': adid,
						'testtype': remarktype,
					},
				}
				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('getAdditionById success ', response)

				if(response.data !== null && response.data.length > 0) return this.ObjectLoopDecodeJson(response.data[0])
				else return null
			} catch (error) {
				return error
			}
		},
		async getCompareList (){
			try {
				const config = {
					url: localStorage.getItem('apiurl') + 'comparelist',
					method: 'GET',
					headers: {
						Authorization: 'Bearer ' + this.userToken
					},
					params: {
						//'rmid': adid,
						//'cgid': cgid,
					},
				}
				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('getAdditionById success ', response)

				if(response.data !== null && response.data.length > 0) return this.ObjectLoopDecodeJson(response.data[0])
				else return null
			} catch (error) {
				return error
			}
		},
		async getCompareById (cgid){
			try {
				const config = {
					url: localStorage.getItem('apiurl') + 'comparelist',
					method: 'GET',
					headers: {
						Authorization: 'Bearer ' + this.userToken
					},
					params: {
						//'rmid': adid,
						'cgid': cgid,
					},
				}
				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('getAdditionById success ', response)

				if(response.data !== null && response.data.length > 0) return this.ObjectLoopDecodeJson(response.data[0])
				else return null
			} catch (error) {
				return error
			}
		},
		async uploadPictureResult (image, testno, reportno) {
			try {
				testno = this.FilterReportNo_AAA_BB_C(testno)
				reportno = this.FilterReportNo_AAA_BB_Cxx(reportno)

				var bodyFormData = new FormData()
				bodyFormData.set('myFile', image, image.name)
				bodyFormData.set('test_no', testno)
				bodyFormData.set('report_no', reportno)

				const config = {
					url: localStorage.getItem('apiurl') + "uploadoption",
					method: 'POST',
					headers: {
						Authorization: 'Bearer ' + this.userToken
					},
					data: bodyFormData
				}
				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('uploadPictureResult ', response)

				if(response.data !== null && response.data.status === "success") 
				{
					return response.data
				}
				else {
					return {status: "fail"}
				}
			} catch (error) {
				return error
			}
		},
		async getPictureResultList (testno, reportno) {
			try {
				const config = {
					url: localStorage.getItem('apiurl') + "uploadoptionlist",
					method: 'POST',
					headers: {
						Authorization: 'Bearer ' + this.userToken
					},
					params: {
						'test_no': testno,
						'report_no': reportno,
					},
				}
				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('getPictureResultList ', response)

				if(response.data !== null) return this.ObjectLoopDecodeJson(response.data)
				else return []
			} catch (error) {
				return error
			}
		},
		async removePictureResult (psid) {
			try {
				const config = {
					url: localStorage.getItem('apiurl') + "uploadoptiondel",
					method: 'POST',
					headers: {
						Authorization: 'Bearer ' + this.userToken
					},
					params: {
						'psid': psid,
					},
				}
				console.log('config', config)
				const response = await HTTP.request(config)
				console.log('removePictureResult ', response)

				if(response.data !== null && response.data.status === "success") 
				{
					return response.data
				}
				else {
					return {status: "fail"}
				}
			} catch (error) {
				return error
			}
		},
		clone(obj){
            if(obj == null || typeof(obj) !== 'object')
                return obj;

            var temp = new obj.constructor(); 
            for(var key in obj)
                temp[key] = this.clone(obj[key]);

            return temp;
        },
		/////////////////////////////////////////////////////////////////////////
		/*reportListManageData(report_list){
			var report_list_tmp = []

			for(var i = 0; i < report_list.length; i++)
			{
				var tmp2 = {
					test_no: report_list[i].test_no,
					report_result: []
				}

				tmp2.report_result = [...report_list[i].report_result]

				if(report_list[i].report_result[0].form_type === "BD"){
					var tmp3 = this.bdManageData(tmp2)
					for(var j = 0; j < tmp3.length; j++){
						report_list_tmp.push(tmp3[j])
					}
				}else{

					report_list_tmp.push(tmp2)
				}
			}

			return report_list_tmp
		},
		bdManageData(raw_data){

			var tno = []

			var r = raw_data.report_result[0].result_list[0].bddata[0].detail

			var last_sno = "xxx"
			var last_location = "xxx"

			//Prepare Data
			for(var i = 0; i < r.length; i++)
			{
				if(r[i].simple1_no.includes("\"")) {
					r[i].simple1_no = ""
				}

				if(r[i].location.includes("\"")) {
					r[i].location = ""
				}

				if(r[i].simple1_no === ""){
					r[i].simple1_no = last_sno
				}

				if(r[i].location === ""){
					r[i].location = last_location
				}

				if(!tno.includes(r[i].simple1_no))
				{
					tno.push(r[i].simple1_no)
					last_sno = r[i].simple1_no
					last_location = r[i].location
				}
			}

			//Split Data
			var new_result_group = []

			for(i = 0; i < tno.length; i++)
			{
				var data_result = []
				for(var j = 0; j < r.length; j++)
				{
					if(tno[i] === r[j].simple1_no)
					{
						data_result.push(r[j])
					}
				}
				new_result_group.push(data_result)
			}

			//Rearange Data
			var new_report_data = []
			for(i = 0; i < new_result_group.length; i++)
			{
				var tmp = this.clone(raw_data)
				var rno = new_result_group[i][0].simple1_no +  tmp.report_result[0].form_type
				tmp.report_result[0].report_no = rno
				tmp.report_result[0].result_list[0].bddata[0].detail = new_result_group[i]
				new_report_data.push(tmp)
			}	

			return new_report_data
		},
		*/
	},
	computed: {
		userToken () {
			return localStorage.getItem('tokenkey')
		}
	}
}
////////////////////////////
		/*logOutMain () {
		try {
		  this.loading = true
		  var bodyFormData = new FormData()
		  const config = {
			url: '/user/logout',
			method: 'post',
			data: bodyFormData,
			headers: {
			  'Content-Type': 'multipart/form-data'
			}
		  }
		  axios.request(config)
		  .then((response) => {
			console.log('logout success ',response)
			this.loading = false
			if (response.data.response.code = 10000){
			  localStorage.removeItem('xsamObject')
			  localStorage.removeItem('xsamUserObject')
			  const state = {
				userMode: '',
				xsamUserObject: null,
				userToken: '',
				isPrintPage: false,
				xsamUserNotification: ''
			  }
			  this.$store.replaceState(JSON.parse(JSON.stringify(state)))
			  this.$router.push('/login')
			}
		  })
		  .catch((error) => {
			if (error.response) {
			  alert("B",error.response.data.response.message)
			} else if (error.request) {
			  console.log('error.request',error.request);
			} else {
			  console.log('Error', error.message);
			}
			this.loading = false
		  });
		} catch (error) {
		  console.error(error);
		  this.loading = false
		}
	  },
	  catchError (error){
		  // console.log('catchError',error)
		  if (error.response) {
			  // The request was made and the server responded with a status code
			  // that falls out of the range of 2xx
			  console.log('error.response.data', error.response.data)
			  console.log('error.response.status', error.response.status)
			  console.log('error.response.headers', error.response.headers)
			  if (error.response.data.response.code === 4) {
				this.logOutMain()
			  }
			  else{
				alert(error.response.data.response.message)
			  }
			} else if (error.request) {
			  // The request was made but no response was received
			  // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
			  // http.ClientRequest in node.js
			  console.log('error.request', error.request)
			  alert('The request was made but no response was received')
			} else {
			  // Something happened in setting up the request that triggered an Error
			  console.log('Error', error.message)
			  alert(error.message)
			}
	  },
	  async getCategories () {
		try {
		  // this.departmentTableLoading = true
		  const config = {
		  url: '/user/getcategorylist',
		  method: 'POST',
			  headers: {
				  Authorization: this.userToken
			  }
		  }
		  console.log('config', config)
		  const response = await axios.request(config)
		  return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async getParts () {
		try {
		  // this.departmentTableLoading = true
		  const config = {
		  url: '/creator/getpartlist',
		  method: 'POST',
			  headers: {
				  Authorization: this.userToken
			  }
		  }
		  console.log('config', config)
		  const response = await axios.request(config)
		  return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async getExam (type) {
		try {
		  var bodyFormData = new FormData()
		  bodyFormData.set('exam_type', type)
		  // this.departmentTableLoading = true
		  const config = {
			url: '/creator/getexamlist',
			method: 'POST',
			headers: {
				Authorization: this.userToken
			},
			data: bodyFormData
		  }
		  console.log('config', config)
		  const response = await axios.request(config)
		  return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async getCertificate () {
		try {
		  var bodyFormData = new FormData()
		  // bodyFormData.set('exam_type', type)
		  // this.departmentTableLoading = true
		  const config = {
			url: '/creator/getcertificatelist',
			method: 'POST',
			headers: {
				Authorization: this.userToken
			},
			data: bodyFormData
		  }
		  console.log('config', config)
		  const response = await axios.request(config)
		  return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async getCertificateBackground () {
		try {
		  var bodyFormData = new FormData()
		  // bodyFormData.set('exam_type', type)
		  // this.departmentTableLoading = true
		  const config = {
			url: '/creator/getcertificatebglist',
			method: 'POST',
			headers: {
				Authorization: this.userToken
			},
			data: bodyFormData
		  }
		  console.log('config', config)
		  const response = await axios.request(config)
		  return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async uploadBackgroundImage (image) {
		try {
		  var bodyFormData = new FormData()
		  bodyFormData.set('imgbg', image, image.name)
		  // this.departmentTableLoading = true
		  const config = {
			url: '/creator/certificatebgmanage/create',
			method: 'POST',
			headers: {
				Authorization: this.userToken
			},
			data: bodyFormData
		  }
		  console.log('config', config)
		  const response = await axios.request(config)
		  return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async getCourses () {
		try {
			var bodyFormData = new FormData()
			const config = {
				url: '/creator/getcourselist',
				method: 'POST',
				headers: {
					Authorization: this.userToken
				},
				data: bodyFormData
			}
			console.log('config', config)
			const response = await axios.request(config)
			return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async getSuperCreators () {
		try {
			var bodyFormData = new FormData()
			const config = {
				url: '/creator/getsupercreatorlist',
				method: 'POST',
				headers: {
					Authorization: this.userToken
				},
				data: bodyFormData
			}
			console.log('config', config)
			const response = await axios.request(config)
			return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async getCertificateById (certificateId) {
		try {
		  var bodyFormData = new FormData()
		  bodyFormData.set('certificate_id', certificateId)
		  // this.departmentTableLoading = true
		  const config = {
			url: '/creator/getcertificatebyid',
			method: 'POST',
			headers: {
				Authorization: this.userToken
			},
			data: bodyFormData
		  }
		  console.log('config', config)
		  const response = await axios.request(config)
		  return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async getMedias (){
		try {
			 var bodyFormData = new FormData()
			 bodyFormData.set('orderby', 'desc')
			 bodyFormData.set('sortby', 'date')
			 bodyFormData.set('lpp', '20')
			 bodyFormData.set('pindx', '0')
			 const config = {
				 url: '/creator/getmedialist',
				 method: 'POST',
				 headers: {
					 Authorization: this.userToken
				 },
				 data: bodyFormData
			 }
			 console.log('config', config)
			 const response = await axios.request(config)
			 return response
		 } catch (error) {
			 this.catchError(error)
			 return error
		 }
	  },
	  async uploadMedias (image) {
		try {
		  var bodyFormData = new FormData()
		  bodyFormData.set('img', image, image.name)
		  // this.departmentTableLoading = true
		  const config = {
			url: '/creator/mediamanage/create',
			method: 'POST',
			headers: {
				Authorization: this.userToken
			},
			data: bodyFormData
		  }
		  console.log('config', config)
		  const response = await axios.request(config)
		  return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async getNewsBg (){
		try {
			 var bodyFormData = new FormData()
			 bodyFormData.set('orderby', 'desc')
			 bodyFormData.set('sortby', 'date')
			 bodyFormData.set('lpp', '30')
			 bodyFormData.set('pindx', '0')
			 const config = {
				 url: '/admin/getnewsimagelist',
				 method: 'POST',
				 headers: {
					 Authorization: this.userToken
				 },
				 data: bodyFormData
			 }
			 console.log('config', config)
			 const response = await axios.request(config)
			 return response
		 } catch (error) {
			 this.catchError(error)
			 return error
		 }
	  },
	  async uploadNewsBg (image) {
		try {
		  var bodyFormData = new FormData()
		  bodyFormData.set('img', image, image.name)
		  // this.departmentTableLoading = true
		  const config = {
			url: '/admin/newsimagemanage/create',
			method: 'POST',
			headers: {
				Authorization: this.userToken
			},
			data: bodyFormData
		  }
		  console.log('config', config)
		  const response = await axios.request(config)
		  return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async getUserCourseList () {
		try {
		  var bodyFormData = new FormData()
		  const config = {
			  url: '/user/getmycourselist',
			  method: 'POST',
			  headers: {
				  Authorization: this.userToken
			  },
			  data: bodyFormData
		  }
		  console.log('config', config)
		  const response = await axios.request(config)
		  return response
		} catch (error) {
		  this.catchError(error)
		  return error
		}
	  },
	  async getUserCourseByID (courseId) {
		try {
			var bodyFormData = new FormData()
			bodyFormData.set('course_register_id', courseId)
			const config = {
				url: '/user/getmycoursebyid',
				method: 'POST',
				headers: {
					Authorization: this.userToken
				},
				data: bodyFormData
			}
			console.log('config', config)
			const response = await axios.request(config)
			return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async getUserPartByID (courseId, partId) {
		try {
			var bodyFormData = new FormData()
			bodyFormData.set('course_register_id', courseId)
			bodyFormData.set('part_id', partId)
			const config = {
				url: '/user/getmypartbyid',
				method: 'POST',
				headers: {
					Authorization: this.userToken
				},
				data: bodyFormData
			}
			console.log('config', config)
			const response = await axios.request(config)
			return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async getCourseById (courseId) {
		try {
			var bodyFormData = new FormData()
			bodyFormData.set('course_id', courseId)
			const config = {
				url: '/creator/getcoursebyid',
				method: 'POST',
				headers: {
					Authorization: this.userToken
				},
				data: bodyFormData
			}
			console.log('config', config)
			const response = await axios.request(config)
			return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async getSharedCourseById (courseId) {
		try {
			var bodyFormData = new FormData()
			bodyFormData.set('course_id', courseId)
			const config = {
				url: '/creator/getsharecoursebyid',
				method: 'POST',
				headers: {
					Authorization: this.userToken
				},
				data: bodyFormData
			}
			console.log('config', config)
			const response = await axios.request(config)
			return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async getMySharedCourseById (courseId) {
		try {
			var bodyFormData = new FormData()
			bodyFormData.set('course_id', courseId)
			const config = {
				url: '/user/getmysharecoursebyid',
				method: 'POST',
				headers: {
					Authorization: this.userToken
				},
				data: bodyFormData
			}
			console.log('config', config)
			const response = await axios.request(config)
			return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async getBranch () {
		try {
			var bodyFormData = new FormData()
			const config = {
				url: '/user/getbranchlist',
				method: 'POST',
				headers: {
					Authorization: this.userToken
				},
				data: bodyFormData
			}
			console.log('config', config)
			const response = await axios.request(config)
			return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async getUserList () {
		try {
			var bodyFormData = new FormData()
			const config = {
				url: '/admin/getuserlist',
				method: 'POST',
				headers: {
					Authorization: this.userToken
				},
				data: bodyFormData
			}
			console.log('config', config)
			const response = await axios.request(config)
			return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async getUserListByCreator () {
		try {
			var bodyFormData = new FormData()
			const config = {
				url: '/creator/getuserlist',
				method: 'POST',
				headers: {
					Authorization: this.userToken
				},
				data: bodyFormData
			}
			console.log('config', config)
			const response = await axios.request(config)
			return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async getCourseScheduleById (courseId) {
		try {
		  var bodyFormData = new FormData()
		  bodyFormData.set('course_id', courseId)
		  const config = {
			url: '/creator/getcourseschedulebyid',
			method: 'POST',
			headers: {
			  Authorization: this.userToken
			},
			data: bodyFormData
		  }
		  console.log('config', config)
		  const response = await axios.request(config)
		  return response
		} catch (error) {
		  this.catchError(error)
		  return error
		}
	  },
	  async getNotification () {
		try {
			var bodyFormData = new FormData()
			const config = {
				url: '/user/getnotification',
				method: 'POST',
				headers: {
					Authorization: this.userToken
				},
				data: bodyFormData
			}
			console.log('config', config)
			const response = await axios.request(config)
			return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async getMyQuizList () {
		try {
		  var bodyFormData = new FormData()
		  // this.departmentTableLoading = true
		  const config = {
			url: 'user/getmyquizlist',
			method: 'POST',
			headers: {
				Authorization: this.userToken
			},
			data: bodyFormData
		  }
		  console.log('config', config)
		  const response = await axios.request(config)
		  return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async submitQuizFinish (examResultId) {
		try {
		  var bodyFormData = new FormData()
		  bodyFormData.set('exam_result_id', examResultId)
		  // this.departmentTableLoading = true
		  const config = {
			url: 'user/submitquizfinish',
			method: 'POST',
			headers: {
				Authorization: this.userToken
			},
			data: bodyFormData
		  }
		  console.log('config', config)
		  const response = await axios.request(config)
		  return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async getMyExamList () {
		try {
		  var bodyFormData = new FormData()
		  // this.departmentTableLoading = true
		  const config = {
			url: 'user/getmyexamlist',
			method: 'POST',
			headers: {
				Authorization: this.userToken
			},
			data: bodyFormData
		  }
		  console.log('config', config)
		  const response = await axios.request(config)
		  return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async startMyExamById (courseRegisterId, examId) {
		try {
		  var bodyFormData = new FormData()
		  // this.departmentTableLoading = true
		  bodyFormData.set('course_register_id', courseRegisterId)
		  bodyFormData.set('exam_id', examId)
		  const config = {
			url: 'user/getmyexambyid',
			method: 'POST',
			headers: {
				Authorization: this.userToken
			},
			data: bodyFormData
		  }
		  console.log('config', config)
		  const response = await axios.request(config)
		  return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async startTestExam (courseRegisterId, examId, isContinue) {
		try {
		  var bodyFormData = new FormData()
		  // this.departmentTableLoading = true
		  bodyFormData.set('course_register_id', courseRegisterId)
		  bodyFormData.set('exam_id', examId)
		  bodyFormData.set('continue', isContinue)
		  const config = {
			url: 'user/starttestexam',
			method: 'POST',
			headers: {
				Authorization: this.userToken
			},
			data: bodyFormData
		  }
		  console.log('config', config)
		  const response = await axios.request(config)
		  return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async updateAnswerExam (courseRegisterId, partId, examId, examResultId, answerList) {
		try {
		  var bodyFormData = new FormData()
		  // this.departmentTableLoading = true
		  bodyFormData.set('course_register_id', courseRegisterId)
		  bodyFormData.set('part_id', partId)
		  bodyFormData.set('exam_id', examId)
		  bodyFormData.set('exam_result_id', examResultId)
		  bodyFormData.set('answer_list', answerList)
		  const config = {
			url: 'user/updateanswerexam',
			method: 'POST',
			headers: {
				Authorization: this.userToken
			},
			data: bodyFormData
		  }
		  console.log('config', config)
		  const response = await axios.request(config)
		  return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async submitExamFinish (examResultId) {
		try {
		  var bodyFormData = new FormData()
		  bodyFormData.set('exam_result_id', examResultId)
		  // this.departmentTableLoading = true
		  const config = {
			url: 'user/submitexamfinish',
			method: 'POST',
			headers: {
				Authorization: this.userToken
			},
			data: bodyFormData
		  }
		  console.log('config', config)
		  const response = await axios.request(config)
		  return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async startMyQuizById (courseRegisterId, partId, quizId) {
		try {
		  var bodyFormData = new FormData()
		  // this.departmentTableLoading = true
		  bodyFormData.set('course_register_id', courseRegisterId)
		  bodyFormData.set('part_id', partId)
		  bodyFormData.set('quiz_id', quizId)
		  const config = {
			url: 'user/getmyquizbyid',
			method: 'POST',
			headers: {
				Authorization: this.userToken
			},
			data: bodyFormData
		  }
		  console.log('config', config)
		  const response = await axios.request(config)
		  return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async startTestQuiz (courseRegisterId, partId, quizId, isContinue) {
		try {
		  var bodyFormData = new FormData()
		  // this.departmentTableLoading = true
		  bodyFormData.set('course_register_id', courseRegisterId)
		  bodyFormData.set('part_id', partId)
		  bodyFormData.set('quiz_id', quizId)
		  bodyFormData.set('continue', isContinue)
		  const config = {
			url: 'user/starttestquiz',
			method: 'POST',
			headers: {
				Authorization: this.userToken
			},
			data: bodyFormData
		  }
		  console.log('config', config)
		  const response = await axios.request(config)
		  return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async updateAnswerQuiz (courseRegisterId, partId, quizId, examResultId, answerList) {
		try {
		  var bodyFormData = new FormData()
		  // this.departmentTableLoading = true
		  bodyFormData.set('course_register_id', courseRegisterId)
		  bodyFormData.set('part_id', partId)
		  bodyFormData.set('quiz_id', quizId)
		  bodyFormData.set('exam_result_id', examResultId)
		  bodyFormData.set('answer_list', answerList)
		  const config = {
			url: 'user/updateanswerquiz',
			method: 'POST',
			headers: {
				Authorization: this.userToken
			},
			data: bodyFormData
		  }
		  console.log('config', config)
		  const response = await axios.request(config)
		  return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async startLearningPart (courseRegisterId, partId) {
		try {
		  var bodyFormData = new FormData()
		  // this.departmentTableLoading = true
		  bodyFormData.set('course_register_id', courseRegisterId)
		  bodyFormData.set('part_id', partId)
		  const config = {
			url: 'user/startlearningpart',
			method: 'POST',
			headers: {
				Authorization: this.userToken
			},
			data: bodyFormData
		  }
		  console.log('config', config)
		  const response = await axios.request(config)
		  return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async updateCountMinutePart (courseRegisterId, partId, addMinute) {
		try {
		  var bodyFormData = new FormData()
		  // this.departmentTableLoading = true
		  bodyFormData.set('course_register_id', courseRegisterId)
		  bodyFormData.set('part_id', partId)
		  bodyFormData.set('add_count_minute', addMinute)
		  const config = {
			url: '/user/updatecountminutepart',
			method: 'POST',
			headers: {
				Authorization: this.userToken
			},
			data: bodyFormData
		  }
		  console.log('config', config)
		  const response = await axios.request(config)
		  return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async getMyCourseScheduleList () {
		try {
		  var bodyFormData = new FormData()
		  const config = {
			url: '/user/getmycourseschedulelist',
			method: 'POST',
			headers: {
				Authorization: this.userToken
			},
			data: bodyFormData
		  }
		  console.log('config', config)
		  const response = await axios.request(config)
		  return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async setUserInformation (email, mobile, oldPass, newPass) {
		try {
		  var bodyFormData = new FormData()
		  bodyFormData.set('email', email)
		  bodyFormData.set('mobile', mobile)
		  bodyFormData.set('language', 'th')
		  bodyFormData.set('old_password', oldPass)
		  bodyFormData.set('password', newPass)
		  const config = {
			url: '/user/setuserinformation',
			method: 'POST',
			headers: {
				Authorization: this.userToken
			},
			data: bodyFormData
		  }
		  console.log('config', config)
		  const response = await axios.request(config)
		  return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async getMyUnderlingList () {
		try {
			var bodyFormData = new FormData()
			const config = {
				url: '/user/getmyunderlinglist',
				method: 'POST',
				headers: {
					Authorization: this.userToken
				},
				data: bodyFormData
			}
			console.log('config', config)
			const response = await axios.request(config)
			return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async getMyCourseScheduleById (courseId) {
		try {
		  var bodyFormData = new FormData()
		  // this.departmentTableLoading = true
		  bodyFormData.set('course_id', courseId)
		  const config = {
			url: 'user/getmycourseschedulebyid',
			method: 'POST',
			headers: {
				Authorization: this.userToken
			},
			data: bodyFormData
		  }
		  console.log('config', config)
		  const response = await axios.request(config)
		  return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async courseMyScheduleUpdateAppointment (userList, appointDate) {
		try {
		  var bodyFormData = new FormData()
		  // this.departmentTableLoading = true
		  bodyFormData.set('course_register_select_list', userList)
		  bodyFormData.set('appointment_date', appointDate)
		  const config = {
			url: 'user/coursemyscheduleupdateappointment',
			method: 'POST',
			headers: {
				Authorization: this.userToken
			},
			data: bodyFormData
		  }
		  console.log('config', config)
		  const response = await axios.request(config)
		  return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async courseScheduleUpdateAppointment (userList, appointDate) {
		try {
		  var bodyFormData = new FormData()
		  // this.departmentTableLoading = true
		  bodyFormData.set('course_register_select_list', userList)
		  bodyFormData.set('appointment_date', appointDate)
		  const config = {
			url: 'creator/coursescheduleupdateappointment',
			method: 'POST',
			headers: {
				Authorization: this.userToken
			},
			data: bodyFormData
		  }
		  console.log('config', config)
		  const response = await axios.request(config)
		  return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async uploadUserImage (image) {
		try {
		  var bodyFormData = new FormData()
		  bodyFormData.set('img', image, image.name)
		  // this.departmentTableLoading = true
		  const config = {
			url: '/user/uploadmyimage',
			method: 'POST',
			headers: {
				Authorization: this.userToken
			},
			data: bodyFormData
		  }
		  console.log('config', config)
		  const response = await axios.request(config)
		  return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  async getUserImage () {
		try {
		  var bodyFormData = new FormData()
		  // this.departmentTableLoading = true
		  const config = {
			url: '/getmedia/getmyimage',
			method: 'GET',
			headers: {
				Authorization: this.userToken
			},
			responseType: 'blob',
			data: bodyFormData
		  }
		  console.log('config', config)
		  const response = await axios.request(config)
		  return response
		} catch (error) {
			this.catchError(error)
			return error
		}
	  },
	  scrollToTop () {
		window.scrollTo(0, 0)
	  }
	},*/