import './plugins/vue-head'
import './plugins/apexchart'
// FILTERS
import './filters/capitalize'
import './filters/lowercase'
import './filters/uppercase'
import './filters/placeholder'
import './filters/trim'
import './filters/formatDate'
// STYLES
// Main Theme SCSS
import './assets/scss/theme.scss'

import Vue from 'vue'

import App from './App.vue'
import apicall from './plugins/apicall'
// PLUGINS

import vuetify from './plugins/vuetify'
// VUE-ROUTER - https://router.vuejs.org/
import router from './router'
// VUEX - https://vuex.vuejs.org/
import store from './store'

// Set this to false to prevent the production tip on Vue startup.
Vue.config.productionTip = false

Vue.mixin(apicall)

/*
|---------------------------------------------------------------------
| Main Vue Instance
|---------------------------------------------------------------------
|
| Render the vue application on the <div id="app"></div> in index.html
|
| https://vuejs.org/v2/guide/instance.html
|
*/
export default new Vue({
	vuetify,
	router,
	store,
	render: (h) => h(App)
}).$mount('#app')
