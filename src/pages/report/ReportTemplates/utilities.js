/* eslint-disable space-before-blocks */
/* eslint-disable indent */

export default {
    methods: {
        FilterDateTime(str) {
            var str_arr = str.split(" ");
            var str_arr2 = str.split("/");

            var d = new Date(),
            //minutes = d.getMinutes().toString().length == 1 ? '0'+d.getMinutes() : d.getMinutes(),
            //hours = d.getHours().toString().length == 1 ? '0'+d.getHours() : d.getHours(),
            //ampm = d.getHours() >= 12 ? 'pm' : 'am',
            months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
            days = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
            //return days[d.getDay()]+' '+months[d.getMonth()]+' '+d.getDate()+' '+d.getFullYear()+' '+hours+':'+minutes+ampm;

            return str_arr2[0]+" "+months[str_arr2[1]-1]+" "+(parseInt(str_arr2[2])-543)
        },
        FilterBlank(str) {
            if(!str || str === ""){
                return "-"
            }
            return str
        },

        GetNumberFromBeforeString(text) {
            var result = "";
            for (var i = 0; i < text.length; i++) {
                var code = text.charCodeAt(i)
                if (code >= 48 && code <= 57) //code 0 - 9
                {
                    result += text[i];
                }
                else
                {
                    break;
                }
            }

            return result;
        },

        GetStringFromAfterNumber(text) {
            var result = "";

            for (var i = 0; i < text.length; i++) {
                var code = text.toUpperCase().charCodeAt(i)
                if(code >= 65 && code <= 90) // code A - Z
                {
                    for (var j = i; j < text.length; j++) {
                        result += text[j]
                    }
                    return result
                }
            }

            return result;
        },
        

        FilterReportNo_01(str) {
            var str_arr = str.split("-");
            if(str_arr.length <= 2) return str_arr[0] + "-" + str_arr[1]
            var res = this.GetNumberFromBeforeString(str_arr[2])
            if(res === "") 
                return str_arr[0] + "-" + str_arr[1]

            return str_arr[0] + "-" + str_arr[1] + "-" + res
        },
        FilterReportNo_02(str) {
            var str_arr = str.split("-");
            if(str_arr.length <= 2) return ""
            var res = this.GetStringFromAfterNumber(str_arr[2])
            return res
        },
        FilterReportNo_02_2(form_type) {

            var form_list = [
                ["TSW", "TS"],
                ["TSWG1", "TS"],
                ["TSB1", "TS"],
                ["TSB1G1", "TS"],
                ["TSB2", "TS"],
                ["TSB2G1", "TS"],
                ["TSB3", "TS"],
                ["TSB3G1", "TS"],
                ["TSPL", "TS"],
                ["TSPLG1", "TS"],

                ["BD", "BD"],
                ["BDMAT", "BD"],

                ["IM", "IM"],

                ["ES", "ES"],
                ["ESSTD", "ES"],
                ["ESAERO", "ES"],

                ["HV3COL", "HV"],
                ["HV4COL", "HV"],
            ]

            for(var i = 0; i < form_list.length; i++)
            {
                if(form_type === form_list[i][0]) return form_list[i][1]
            }

            return form_type

            /*var str_arr = str.split("-");

            var form_list = [
                ["TS", "TS"],
                ["BD", "BD"],
                ["FTN", "FTN"],
                ["NB", "NB"],
                ["FT", "FT"],
                ["IM", "IM"],
                ["MA", "MA"],
                ["ES", "ES"],
                ["MS", "MS"],
                ["HV", "HV"],
                ["HR", "HR"],
                ["HB", "HB"],
                ["MI", "MI"],
                ["PC", "PC"],
                ["IC", "IC"],
                ["FC", "FC"],
                ["MM", "MM"],
                ["DM", "DM"] ]
            var str_tmp = String(str_arr[2])
            for(var i = 0; i < form_list.length; i++)
            {
                if(str_tmp.includes(form_list[i][0])) return form_list[i][1]
            }

            return str_tmp*/
        },
        BD_SpecType(form_type2) {
            let text = "Blue has a blue house"
            if( form_type2.search("FB") >= 0) return "Face bend"
            else if( form_type2.search("FB") >= 0) return "Face bend"
            else if( form_type2.search("RB") >= 0) return "Root bend"
            else if( form_type2.search("SB") >= 0) return "Side bend"
            else if( form_type2.search("BD") >= 0) return "Base metal"
            return "-"
        },
    },
}
